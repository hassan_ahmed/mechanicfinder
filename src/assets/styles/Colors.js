import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {screenHeight, screenWidth, images} from '../../config/Constant';
export default StyleSheet.create({
  whiteLayer: {
    color: 'rgba(255, 255, 255, 0.2)',
  },
  orablu: {color: ['#F59E52', '#482C6D']},
  inboxBarInActive: {color: ['#fff', '#fff']},
  inboxBarActive: {color: ['#6B379D', '#2B2F92']},
  inputBordercolor: {color: '#9B9B9B'},
  darkBlue: {color: '#321755'},
  purple: {color: '#381C5C'},
  solidwhite: {color: '#FFFFFF'},
  gray: {color: '#9B9B9B'},
  heading2light: {color: 'rgba(57,62,70,0.6)'},
  fadewhite: {color: 'rgba(255,255,255,0.3)'},
  dullBlack: {color: 'rgba(0,0,0,0.4)'},
  darkyellow: {color: '#F59E52'},
  black: {color: 'black'},
  lightblue: {color: '#0C233C'},
  Black323: {color: '#323643'},
  gray666: {color: '#676666'},
});
